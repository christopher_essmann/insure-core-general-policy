provider "aws" {
  region = "eu-central-1"

  assume_role {
    role_arn = format(
      "arn:aws:iam::%s:role/automation/Provisioner", var.aws_account_number
    )
  }
}
